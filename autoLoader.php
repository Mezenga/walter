<?php
spl_autoload_register('AutoLoader::rootLoader');
spl_autoload_register('AutoLoader::UserModelLoader');

class AutoLoader
{
    /**
     * Load all files from project root
     * @param $className
     */
    public static function rootLoader($className)
    {
        $path = $_SERVER['DOCUMENT_ROOT'].'/autoLoadTest/'.$className.'.php';
        if( file_exists($path))
        {
            require_once $path;
        }
    }

    /**
     * Load all files from models folder
     * @param $className
     */
    public static function UserModelLoader($className)
    {
        $path = $_SERVER['DOCUMENT_ROOT'].'/autoLoadTest/'.'models/'.$className.'.php';
        if( file_exists($path))
        {
            require_once $path;
        }
    }
}