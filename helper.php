<?php


class helper {

    public static function resolvePagination()
    {
        $recordsOnPage = 5;
        if($_GET['page'])
        {
           $page = $_GET['page'];
        }
        else
        {
            $page = 1;
        }

        $skip = ($page - 1) * $recordsOnPage;

        return array('skip' => $skip, 'recordsOnPage' => $recordsOnPage);
    }

    public static function resolveRequest()
    {
        switch($_SERVER['REQUEST_METHOD'])
        {
            case 'GET': $request = &$_GET; break;
            case 'POST': $request = &$_POST; break;
            default: break;
        }

        return $request;
    }


} ?>