<?php
require_once $_SERVER['DOCUMENT_ROOT'].'/autoLoadTest/autoLoader.php';

//pick up request
$helperClass = new helper();
$request = $helperClass->resolveRequest();

$modelClass = $request['model'];
$model = new $modelClass();
$response['inserted'] = $model->save($request);

print_r($response);
