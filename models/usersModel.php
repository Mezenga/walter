<?php
class usersModel {

    public function getAllUsers()
    {
        $sql = 'SELECT * FROM users';

        $query = database::connect()->query($sql);

        $result = $query->fetchAll(PDO::FETCH_ASSOC);

        return $result;
    }

    public function getSpecificUser($id)
    {
        $sql = "SELECT * FROM users WHERE id=:id";

        $query = database::connect()->prepare($sql);
        $query->execute(array(':id' => $id));

        $result = $query->fetch(PDO::FETCH_ASSOC);

        return $result;
    }

    public function save($data)
    {
        if(!isset($data['id']))
        {
            $sql = 'INSERT INTO users SET username=:username, password=:password';
            $row = database::connect()
                ->prepare($sql)
                ->execute(array(':username' => $data['username'],':password' =>$data['password']));
            return $row;
            database::disconnect();
        }
    }


}