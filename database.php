<?php

class database
{
    //database credentials
    private static $DB_host = "localhost:3306";
    private static $DB_user = "localUser";
    private static $DB_pass = "branisarajevo";
    private static $DB_name = "test_db";

    //Instanca konekcije na bazu
    private static $conn;


    //Jedna funkcija za konekciju u cijeloj aplikaciji
    public static function connect()
    {
        try
        {
            if (null == self::$conn)
            {
                self::$conn = new PDO("mysql:host=" . self::$DB_host . ";" . "dbname=" . self::$DB_name, self::$DB_user, self::$DB_pass);
                self::$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                self::$conn->query('set character_set_client=utf8');
                self::$conn->query('set character_set_connection=utf8');
                self::$conn->query('set character_set_results=utf8');
                self::$conn->query('set character_set_server=utf8');
            }
        }
        catch(PDOException $ex)
        {
            die($ex->getMessage());
        }
        return self::$conn;
    }
    //funkcija za oslobadjanje konekcije
    public static function disconnect()
    {
        self::$conn = null;
    }
}