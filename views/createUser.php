<?php
$request = helper::resolveRequest();


if(isset($request['id']))
{
    $userModel = new usersModel();
    $user = $userModel->getSpecificUser($request['id']);
}
?>
<form method="post" id="create_user" action="">
    <input name="username" type="text" placeholder="Enter name" value="<?= $user ? $user['username'] : ''?>">
    <input name="password" type="password" placeholder="Enter name" value="<?= $user ? $user['password'] : ''?>>
    <button class="btn btn-primary" id="insert_user" name="insert_user" type="submit"> Create user</button>
    <input style="display: none" name="model" value="usersModel">
</form>

<script>
    $(document).ready(function(){
        $('#create_user').submit(function (event) {
            event.preventDefault();
            $.ajax({
                type: "POST",
                url: '<?= FULL_URL_PATH.'models/ajaxSave.php' ?>',
                data: $(this).serialize(),
                success: function (response) {
                    console.log(response);
                }
            });
        });
    });
</script>