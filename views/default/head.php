<head>
    <title>Test app</title>
    <meta charset="utf-8">

    <script
        src="https://code.jquery.com/jquery-3.3.1.js"
        integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60="
        crossorigin="anonymous"></script>
    <link href='<?php echo FULL_URL_PATH;?>assets/bootstrap/css/bootstrap.css' rel="stylesheet" type="text/css" media="all"/>
    <script src="<?php echo FULL_URL_PATH;?>assets/bootstrap/js/bootstrap.js"></script>
</head>
<body>