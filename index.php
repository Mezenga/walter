<?php

//ini_set('display_errors', 1);
//ini_set('display_startup_errors', 1);
//error_reporting(E_ALL);

header('Content-Type: text/html; charset=utf-8');
// Absolute path to  installation, result: /www/webvol2/qj/asb31ymi8exutp6/avdalovic.walter-dev.com/public_html/itp_projekat
$base_dir  = __DIR__;
// Removes a script dir name from full path ,result: /www/webvol2/qj/asb31ymi8exutp6/avdalovic.walter-dev.com/public_html
$doc_root  = preg_replace("!${_SERVER['SCRIPT_NAME']}!", '', $_SERVER['SCRIPT_FILENAME']);
// When $doc_root is replaced with '' in $base_dir, path to current folder  is obtained, result '/itp_projekat'
$base_url  = preg_replace("!${doc_root}!", '', $base_dir);
//Getting a protocol based on variable $_SERVER['HTTPS'] variable
$protocol  = empty($_SERVER['HTTPS']) ? 'http' : 'https';
//Obtaining a port number from $_SERVER[SERVER_PORT]
$port      = $_SERVER['SERVER_PORT'];
//If protocol is https returns :443 for accessing web site
$curr_port = ($protocol == 'http' && $port == 80 || $protocol == 'https' && $port == 443) ? '' : ":$port";
// Returns domain name, result: avdalovic.walter-dev.com
$domain    = $_SERVER['SERVER_NAME'];

/**
 * Full url is obtained from appending $protocol variable with '://' for "http://"'
 * Appending is continued with concatenating variable $domain, then variable $curr_port and then variable $base_url to $full_url
 **/
$full_url  = "${protocol}://${domain}${curr_port}${base_url}"; // Result: http://avdalovic.walter-dev.com/itp_projekat
$full_url .= '/';
$full_file_path = $_SERVER['DOCUMENT_ROOT'] . '/autoLoadTest/';

$GLOBALS['full_url_path'] = $full_url;
$GLOBALS['full_file_path'] = $full_file_path;

define("FULL_URL_PATH",$full_url);
define("FULL_FILE_PATH", $full_file_path);

require_once FULL_FILE_PATH.'autoLoader.php';

$helperClass = new helper();

$request = $helperClass->resolveRequest();

include FULL_FILE_PATH."views/default/head.php";
include FULL_FILE_PATH."views/default/header.php";
?>
<div class="container">

<?php
if(isset($request['view']))
{
    $page = $request['view'];
    include FULL_FILE_PATH. "views/".$page.".php";
}
?>
</div>
<footer class="footer">
<?php
include FULL_FILE_PATH."views/default/footer.php";
?>
</footer>



